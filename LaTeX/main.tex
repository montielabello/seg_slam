\documentclass[a4paper,11pt]{article}
\usepackage[%
  papersize={210mm,297mm},%
  layoutsize={200mm,270mm},%
  layoutoffset=5mm,%
  %twoside,%
  nomarginpar,%
  inner=20mm,%
  outer=20mm,%
  vmargin=20mm,%
  voffset=0mm,
  head=10mm,%
  headsep=10mm,%
  %includefoot,
  foot=15mm,%
  %includefoot,
  %showcrop,%
  %showframe,%
]{geometry}

\usepackage[pdftex, final]{graphicx}
\graphicspath{{./pics/}}

\usepackage{enumitem}
\usepackage{float}

\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{tensor}

\usepackage{titling}
\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}}%
}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[hidelinks]{hyperref}
\usepackage{amsmath}
\DeclareMathOperator*{\argmax}{argmax} % thin space, limits underneath in displays


\setcounter{tocdepth}{6}
\setcounter{secnumdepth}{6}
%\setcounter{chapter}{1}
%\usepackage{enumitem}
%\setlist{nosep} % or \setlist{noitemsep} to leave space around whole list

\usepackage{graphicx}
\newcommand*{\matminus}{%
  \leavevmode
  \hphantom{0}%
  \llap{%
    \settowidth{\dimen0 }{$0$}%
    \resizebox{1.1\dimen0 }{\height}{$-$}%
  }%
}


%tikz
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{verbatim}
\usepackage{amsmath}

\newcommand{\argmin}{\arg\!\min}

\usetikzlibrary{arrows,positioning} 
\tikzset{
    %Define standard arrow tip
    >=stealth',
    %Define style for boxes
    punkt/.style={
           rectangle,
           rounded corners,
           draw=black, very thick,
           text width=6.5em,
           minimum height=2em,
           text centered},
    % Define arrow style
    pil/.style={
           ->,
           thick,
           shorten <=2pt,
           shorten >=2pt,}
}

%--------------------------------------------------------------
\begin{document}

\title{Segmentation and SLAM}
%\subtitle{}
%\date{March 11th, 2019}
\author{Montiel Abello}
\maketitle

Autonomous robots require both a geometric and semantic representation of the environment in order to navigate within it and act upon it in a useful manner. The capability to understand \textit{what} a robot is observing has been improved by advances in instance segmentation \cite{He17iccv,Chen19iccv,Hou19cvpr}. The state-of-the-art methods in instance segmentation have developed as a combination of deep object detection and semantic segmentation systems. This “detect and label” approach to segmentation has been incorporated in recent works in object-SLAM and dense reconstruction \cite{Mccormac183dv,Runz18ismar,Runz20cvpr}. Fusion++ \cite{Mccormac183dv} is an RGB-D SLAM system that uses \cite{He17iccv} to identify objects which are used as landmarks in pose graph optimization. Knowledge of objects improves the robustness of loop closures and reduces warping in their dense reconstruction. In FroDO \cite{Runz20cvpr}, dense reconstruction is achieved from sparse RGB images by detecting objects and expressing these observations with a compressed shape encoding which is used in a multi-view optimization to estimate object shape and pose.

For application to a wider range of environments and tasks this object-centric approach is limiting. State-of-the-art instance segmentation methods based on object detection often fail to identify thin and small objects that are not well observed, as well as foreign objects that have not been encountered in the training process.
This limitation is addressed in \cite{Kirillov19cvpr}, where \textit{panoptic segmentation} refers to the objective of assigning all data to an object instance or amorphous region category. \cite{Pham18eccv,Pham18icra} perform segmentation with an open-set assumption, meaning that objects of known and unknown class are identified. If methods using detection based segmentation can be viewed as treating objects sparsely, these panoptic, open-set methods can be considered the dense estimation analogue. 

RPL is currently investigating 3D reconstruction of thin and small objects. In this setting, thin objects are smoothed over in the depth pre-processing that a LiDAR sensor performs, and non-reflective objects are not observed. It is clear that it is essential to take into account the effect of object material and geometry when processing raw sensor data.

This project aims to extend this work in thin and small object reconstruction to develop an object-centric SLAM system with a panoptic, open-set treatment of objects and regions. As segmentation is itself a challenging problem that is aided by accurate geometry, object segmentation and reconstruction will be optimized jointly. Object boundaries and semantic labels will improve the accuracy and robustness of the SLAM solution and offer increased capabilities for robots interacting with the environment. Accurate, complete segmentation will allow for the identification of challenging object types in terms of geometry and material, which can be better reconstructed with prior models. These objects can also be excluded from use in tracking for improved station estimation and loop closure. Efficiency can be improved by reconstructing objects to a desired level of detail, as determined by their class.

\bibliographystyle{IEEEtran}
\bibliography{references,references_seg}

\end{document}     


